<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\History;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;

class HistoryController extends AbstractController
{
    /**
     * @Route("/history", methods={"GET"})
     */
    public function index() {
        $repository = $this->getDoctrine()->getRepository(History::class);
        $history = $repository->findBy([],['created' => 'desc']);
        $serializedEntity = $this->container->get('serializer')->serialize($history, 'json');
        return new Response($serializedEntity);
    }

    /**
     * @Route("/history", methods={"POST"})
     * @param Request $request
     */
    public function store(Request $request, LoggerInterface $logger)
    {
        $url = json_decode($request->getContent())->url;
        $entityManager = $this->getDoctrine()->getManager();
        $history = new History();
        $history->setName($this->getTitle($url));
        $history->setUrl($url);
        $history->setBookmarked(false);

        // tell Doctrine you want to (eventually) save the history (no queries yet)
        $entityManager->persist($history);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        $serializedEntity = $this->container->get('serializer')->serialize($history, 'json');

        return new Response($serializedEntity);
    }

    /**
     * @Route("/bookmarks", methods={"GET"})
     */
    public function getBookmarks()
    {
        $repository = $this->getDoctrine()->getRepository(History::class);
        $bookmarks = $repository->findBy(['bookmarked' => true],['created' => 'desc']);
        $serializedEntity = $this->container->get('serializer')->serialize($bookmarks, 'json');

        return new Response($serializedEntity);
    }

    /**
     * @Route("/bookmarks", methods={"POST"})
     * @param Request $request
     */
    public function storeBookmarks(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository(History::class);
        $bookmark = $repository->findOneBy(['url' => json_decode($request->getContent())->url]);
        if ($bookmark) {
            $bookmark->setBookmarked(true);
            $entityManager->flush();
        } else {
            $url = json_decode($request->getContent())->url;
            $bookmark = new History();
            $bookmark->setName($this->getTitle($url));
            $bookmark->setUrl($url);
            $bookmark->setBookmarked(true);
            $entityManager->persist($bookmark);
            $entityManager->flush();
        }
        $serializedEntity = $this->container->get('serializer')->serialize($bookmark, 'json');
        return new Response($serializedEntity);
    }

    public function getTitle($url) {
        $id = substr($url, -11);
        $content = file_get_contents("http://youtube.com/get_video_info?video_id=".$id);
        parse_str($content, $ytarr);
        $title = $ytarr['title'];
        return $title;
    }
}
